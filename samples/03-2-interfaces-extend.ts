
interface String {
    format(...args): string;
}

String.prototype.format = function (...args) {
    return this.replace(/{(\d+)}/g, function (match, number) {
        return typeof args[number] != 'undefined'
            ? args[number]
            : match
            ;
    });
};

document.body.innerHTML = "{0} {1}".format("hello!", 1);