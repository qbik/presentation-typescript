namespace a1 {

interface IStudent {
    fullName: string;
    name : number;
}

class Student implements IStudent {
    fullName: string;
    name: any;
    something;
    // the use of `public` on arguments to the constructor is a shorthand that allows us to automatically create properties with that name.
    constructor(public firstName, public middleInitial, public lastName) {
        this.fullName = firstName + " " + middleInitial + " " + lastName;
    }

    write() {
        console.log(this.fullName);
        this.name = "sds";
    }
}


var s = new Student("n","a","b");
s.something = "1";
s.something =1;




class SchoolStudent extends Student {
    age : number;
}


class FileInfo {
    fullName: string;
    name: string;
}


function greeter4(person : Person) {
    return "Hello, " + person.firstName + " " + person.lastName;
}

var student = new Student("Jane", "M.", "User");

document.body.innerHTML = greeter4(student);

var schoolStudent = new SchoolStudent("Sylvester", "", "Stalone");
schoolStudent.age = 12;

student = schoolStudent;

// fails
// schoolStudent = student;

var istudent : IStudent = schoolStudent;
//istudent = new FileInfo();

// this fails: 
// istudent = "student";

}