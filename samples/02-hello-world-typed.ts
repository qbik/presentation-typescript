
namespace a1 {
// added function argument typing

function greeter2(person: string) {
    return "Hello, " + person;
}


var user2 = "user";

// error TS2345: Argument of type 'number' is not assignable to parameter of type 'string'.
// var user2 = 1;

document.body.innerHTML = greeter2(user2);
greeter2(0);

// Notice that although there were errors, the greeter.js file is still created. You can use TypeScript even if there are errors in your code. But in this case, TypeScript is warning that your code will likely not run as expected.
                    
}