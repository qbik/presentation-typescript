// generic function

function identity<T>(arg: T): T {
    return arg;
}

let output = identity<string>("myString");


// assing generic func to a variable
let myIdentity1: <T>(arg: T) => T = identity;

// We can also write the generic type as a call signature of an object literal type:
let myIdentity2: { <T>(arg: T): T } = identity;

// generic interface 
interface GenericIdentityFn {
    <T>(arg: T): T;
}

let myIdentity3: GenericIdentityFn = identity;

// a REAL generic interface 
interface GenericIdentityFn2<T> {
    (arg: T): T;
}


let myIdentity4: GenericIdentityFn2<number> = identity;

// generic classes 
class GenericNumber<T> {
    zeroValue: T;
    add: (x: T, y: T) => T;
}

let myGenericNumber = new GenericNumber<number>();
myGenericNumber.zeroValue = 0;
myGenericNumber.add = function (x, y) { return x + y; };



// type constraints
interface Lengthwise {
    length: number;
}

function loggingIdentity<T extends Lengthwise>(arg: T): T {
    console.log(arg.length);  // Now we know it has a .length property, so no more error
    return arg;
}

loggingIdentity({ length: 10, value: 3 });

// using class types in generics
function create<T>(c: { new (): T; }): T {
    return new c();
}

// A more advanced example uses the prototype property
class BeeKeeper {
    hasMask: boolean;
}

class ZooKeeper {
    nametag: string;
}

class Animal {
    numLegs: number;
}

class Bee extends Animal {
    keeper: BeeKeeper;
}

class Lion extends Animal {
    keeper: ZooKeeper;
}

function findKeeper<A extends Animal, K>(a: {
    new (): A;
    prototype: { keeper: K }
}): K {

    return a.prototype.keeper;
}

findKeeper(Lion).nametag;  // typechecks!

class Collection<T> {
    private _items : T[] = [] ;
    public GetItem(index: number): T {
        return this._items[index];
    }
}


