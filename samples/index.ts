function loadFile(file: string) {
    document.body.innerHTML = "";
    var s = document.createElement('script');
    s.type = 'text/javascript';
    s.src = file;
    document.body.appendChild(s);
}

window.onload = () => {
    if (window.location.hash != "") {
        var file = window.location.hash.substr(1);
        loadFile(file);
    }
    else {
        var hrefs = document.querySelectorAll("a[data-file]");
        for (var index = 0; index < hrefs.length; index++) {
            var href = <HTMLHRElement>hrefs[index];
            var file = href.getAttribute("data-file");
            href.textContent = file;
            href.setAttribute("href", "#" + file);
        }
    }
}

window.onhashchange = function () {
    console.log("hash changed to: " + window.location.hash);
    if (window.location.hash == "") {
        window.location.reload();
    } else {
        var file = window.location.hash.substr(1);
        loadFile(file);
    }
}