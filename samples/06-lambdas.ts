// change compilerOptions:target from es3 to es2015 and see what happens to generated javascript
setTimeout(() => {
    document.body.innerHTML = "hello from timeout";
}, 100);

(function outerFunc() {
    this.c = 1;
    // this is converted to _this in typescript
    var l = () => {
        'use strict';
        var self = this;
        console.log("lambda called. this=" + this + " self=" + self + " this.c=" + this.c);
    }

    var f = function () {
        var self = this;
        console.log("func called. this=" + this + " self=" + self + " this.c=" + this.c);
    }


    l();
    l.apply("dupa");
    f.apply("dupa");
})();

class c1 {
    c: number = 0;
    test() {
        var l = () => {
            var self = this;
            console.log("lambda called. this=" + this + " self=" + self + " this.c=" + this.c);
        }

        var f = function () {
            var self = this;
            console.log("func called. this=" + this + " self=" + self + " this.c=" + this.c);
        }

        l();
        l.apply("dupa");
        f.apply("dupa");
    }
}

var c = new c1();
c.test(); 