# Typescript
### Use JavaScript and don't go mad

#### <jakub.pawlowski@legimi.com>



# Agenda

* What's TypeScript and Why
* Features
* Setup and Installation
* Demo
* Real-life Usage



# Why - is something wrong with JS?

* Dynamic typing
* lack of modularity
* scales badly


# TypeScript

> "TypeScript is a typed superset of JavaScript that compiles to plain JavaScript"


# Typescript

* Superset of JavaSCript
* Optionally typed
* No special runtime required
* Compiles to ES3/ES5/ES2015 - `transpiler`

![typescript-es6-es5](img/typescript-es6-es5.png)


## State of the art JavaScript

> "TypeScript offers support for the latest and evolving JavaScript features, including those from ECMAScript 2015 and future proposals, like async functions and decorators, to help build robust components."

note: These features are available at development time for high-confidence app development, but are compiled into simple JavaScript that targets ECMAScript 3 (or newer) environments.

## Starts and ends with JavaScript

TypeScript starts from the same syntax and semantics that millions of JavaScript developers know today. Use existing JavaScript code, incorporate popular JavaScript libraries, and call TypeScript code from JavaScript.

TypeScript compiles to clean, simple JavaScript code which runs on any browser, in Node.js, or in any JavaScript engine that supports ECMAScript 3 (or newer).

## Strong tools for large apps

Types enable JavaScript developers to use highly-productive development tools and practices like static checking and code refactoring when developing JavaScript applications.

Types are optional, and type inference allows a few type annotations to make a big difference to the static verification of your code. Types let you define interfaces between software components and gain insights into the behavior of existing JavaScript libraries.




# Installation

INSTALL
> npm install -g typescript

COMPILE
> tsc helloworld.ts



# Demo



# Language


## Type annotations

    function greeter(person: string) {
        return "Hello, " + person;
    }


## Interfaces
    interface Person {
        firstName: string;
        lastName: string;
    }


# Classes
    
    class Animal {
        feet: number;
        constructor(name: string, numFeet: number) { }
    }
   


# Classes

 * Can implement interfaces
 * Inheritance
 * Instance methods/members
 * Static methods/members
 * Single constructor 
 * ES6 class syntax


# Type Compatibility 

> Type compatibility in TypeScript is based on structural subtyping

    interface Named {
        name: string;
    }

    class Person {
        name: string;
    }

    let p: Named;
    // OK, because of structural typing
    p = new Person();


# Casting

    var hrefs = document.querySelectorAll("a"); // NodeListOf<Element>

    for (var index = 0; index < hrefs.length; index++) {
        var href = <HTMLHRElement>hrefs[index];
        href.textContent = "click me!";
    }



# Other stuff    


### Arrow functions
    setTimeout(() => {
        document.body.innerHTML = "hello from timeout";
    }, 100);


### Enums

    enum Status { Ready, Waiting };
    enum Color { Red, Blue, Green };

    let status = Status.Ready;
    status = Color.Green;  //error


### Generics

    class Collection<T> {
        private _items : T[] = [] ;
        public GetItem(index: number): T {
            return this._items[index];
        }
    }



# Integration with JavaScript


# Typings

    npm install -g typings

    typings search jquery

    typings install dt~jquery --save --global


# Extending javascript types 

    interface String {
        format(...args): string;
    }

    String.prototype.format = function () {
        var args = arguments;
        return this.replace(/{(\d+)}/g, function (match, number) {
            return typeof args[number] != 'undefined'
                ? args[number]
                : match
                ;
        });
    };

    document.body.innerHTML ="{0} {1}".format("hello!", 1);


## Use JavaScript scripts

    // declare variables from .js files
    declare var externalVariable;
    declare var externalString: string;  
    declare var externalObject: any;



# Development cycle

* using grunt/gulp
* sourcemaps
* live reload
* uglify/merge



# Questions?


# Deep dive


## Comparing two functions

    let x = (a: number) => 0;
    let y = (b: number, s: string) => 0;

    y = x; // OK
    x = y; // Error


## Comparing two functions

    let x = () => ({name: 'Alice'});
    let y = () => ({name: 'Alice', location: 'Seattle'});

    x = y; // OK
    y = x; // Error because x() lacks a location property


## Function Parameter Bivariance

    enum EventType { Mouse, Keyboard }

    interface Event { timestamp: number; }
    interface MouseEvent extends Event { x: number; y: number }
    interface KeyEvent extends Event { keyCode: number }

    function listenEvent(eventType: EventType, handler: (n: Event) => void) {
        /* ... */
    }


## Function Parameter Bivariance

    // Unsound, but useful and common
    listenEvent(EventType.Mouse, 
        (e: MouseEvent) => console.log(e.x + ',' + e.y)
    );

note: // Undesirable alternatives in presence of soundness
    listenEvent(EventType.Mouse, (e: Event) => console.log((<MouseEvent>e).x + ',' + (<MouseEvent>e).y));
    listenEvent(EventType.Mouse, <(e: Event) => void>((e: MouseEvent) => console.log(e.x + ',' + e.y)));

    // Still disallowed (clear error). Type safety enforced for wholly incompatible types
    listenEvent(EventType.Mouse, (e: number) => console.log(e));


 ## Generics

    interface NotEmpty<T> {
        data: T;
    }
    let x: NotEmpty<number>;
    let y: NotEmpty<string>;

    x = y;  // error, x and y are not compatible

note: let identity = function<T>(x: T): T {
    // ...
    }
    let reverse = function<U>(y: U): U {
        // ...
    }

    identity = reverse;  // Okay because (x: any)=>any matches (y: any)=>any


## Generics

    interface Empty<T> {
    }
    let x: Empty<number>;
    let y: Empty<string>;

    x = y;  // okay, y matches structure of x


# Other Stuff

## Iterators and Generators
## Modules
## Namespaces
## Module Resolution
## Declaration Merging
## Decorators
## Mixins
## Triple-Slash Directives